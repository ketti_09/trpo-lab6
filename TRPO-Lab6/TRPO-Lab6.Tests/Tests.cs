using NUnit.Framework;
using System;

namespace TRPO_Lab6.Tests
{
    public class Tests
    {
        [Test]
        public void Test1VSphere()
        {
            double r = 5;
            double expected = (500 * Math.PI) / 3;
            double res = new TRPO_Lab6.Lib.Lib().VSphere(r);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VSphere()
        {
            double r = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VSphere(r));
        }
        [Test]
        public void Test3VSphere()
        {
            double r = -5;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VSphere(r));
        }
        [Test]
        public void Test1VCylinder()
        {
            double r = 7;
            double h = 4;
            double expected = 196 * Math.PI;
            double res = new TRPO_Lab6.Lib.Lib().VCylinder(r, h);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VCylinder()
        {
            double r = 0;
            double h = 7;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCylinder(r, h));
        }
        [Test]
        public void Test3VCylinder()
        {
            double r = -5;
            double h = 7;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCylinder(r, h));
        }
        [Test]
        public void Test4VCylinder()
        {
            double r = 8;
            double h = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCylinder(r, h));
        }
        [Test]
        public void Test5VCylinder()
        {
            double r = 3;
            double h = -10;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCylinder(r, h));
        }
        [Test]
        public void Test1VCone()
        {
            double r = 4;
            double h = 7;
            double expected = (112 * Math.PI) / 3;
            double res = new TRPO_Lab6.Lib.Lib().VCone(r, h);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VCone()
        {
            double r = 0;
            double h = 3;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCone(r, h));
        }
        [Test]
        public void Test3VCone()
        {
            double r = -4;
            double h = 3;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCone(r, h));
        }
        [Test]
        public void Test4VCone()
        {
            double r = 4;
            double h = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCone(r, h));
        }
        [Test]
        public void Test5VCone()
        {
            double r = 3;
            double h = -9;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VCone(r, h));
        }
        [Test]
        public void Test1VTruncatedCone()
        {
            double r1 = 2;
            double r2 = 9;
            double h = 2;
            double expected = (206 * Math.PI) / 3;
            double res = new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VTruncatedCone()
        {
            double r1 = 0;
            double r2 = 9;
            double h = 9;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h));
        }
        [Test]
        public void Test3VTruncatedCone()
        {
            double r1 = -4;
            double r2 = 2;
            double h = 1;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h));
        }
        [Test]
        public void Test4VTruncatedCone()
        {
            double r1 = 8;
            double r2 = 0;
            double h = 1;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h));
        }
        [Test]
        public void Test5VTruncatedCone()
        {
            double r1 = 7;
            double r2 = -2;
            double h = 4;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h));
        }
        [Test]
        public void Test6VTruncatedCone()
        {
            double r1 = 9;
            double r2 = 6;
            double h = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h));
        }
        [Test]
        public void Test7VTruncatedCone()
        {
            double r1 = 7;
            double r2 = 4;
            double h = -9;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h));
        }
        [Test]
        public void Test1VPyramid()
        {
            double s = 3;
            double h = 8;
            double expected = 8;
            double res = new TRPO_Lab6.Lib.Lib().VPyramid(s, h);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VPyramid()
        {
            double s = 0;
            double h = 3;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VPyramid(s, h));
        }
        [Test]
        public void Test3Pyramid()
        {
            double s = -2;
            double h = 9;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VPyramid(s, h));
        }
        [Test]
        public void Test4VPyramid()
        {
            double s = 5;
            double h = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VPyramid(s, h));
        }
        [Test]
        public void Test5Pyramid()
        {
            double s = 4;
            double h = -22;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VPyramid(s, h));
        }
        [Test]
        public void Test1VTruncatedPyramid()
        {
            double s1 = 6;
            double s2 = 5;
            double h = 6;
            double expected = 22 + 2 * Math.Sqrt(30);
            double res = new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VTruncatedPyramid()
        {
            double s1 = 0;
            double s2 = 5;
            double h = 8;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h));
        }
        [Test]
        public void Test3VTruncatedPyramid()
        {
            double s1 = -2;
            double s2 = 4;
            double h = 1;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h));
        }
        [Test]
        public void Test4VTruncatedPyramid()
        {
            double s1 = 7;
            double s2 = 0;
            double h = 9;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h));
        }
        [Test]
        public void Test5VTruncatedPyramid()
        {
            double s1 = 4;
            double s2 = -9;
            double h = 4;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h));
        }
        [Test]
        public void Test6VTruncatedPyramid()
        {
            double s1 = 3;
            double s2 = 2;
            double h = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h));
        }
        [Test]
        public void Test7VTruncatedPyramid()
        {
            double s1 = 3.4;
            double s2 = 4.2;
            double h = -9.2;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h));
        }
        [Test]
        public void Test1V�ube()
        {
            double h = 6;
            double expected = 216;
            double res = new TRPO_Lab6.Lib.Lib().V�ube(h);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2V�ube()
        {
            double h = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().V�ube(h));
        }
        [Test]
        public void Test3V�ube()
        {
            double h = -1;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().V�ube(h));
        }
        [Test]
        public void Test1VParallelepiped()
        {
            double a = 8;
            double b = 3;
            double c = 8;
            double expected = 192;
            double res = new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c);
            Assert.AreEqual(expected, res);
        }
        [Test]
        public void Test2VParallelepiped()
        {
            double a = 0;
            double b = 4;
            double c = 7;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c));
        }
        [Test]
        public void Test3VParallelepiped()
        {
            double a = -1;
            double b = 3;
            double c = 6;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c));
        }
        [Test]
        public void Test4VParallelepiped()
        {
            double a = 9;
            double b = 0;
            double c = 3;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c));
        }
        [Test]
        public void Test5VParallelepiped()
        {
            double a = 3;
            double b = -8;
            double c = 4;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c));
        }
        [Test]
        public void Test6VParallelepiped()
        {
            double a = 6;
            double b = 9;
            double c = 0;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c));
        }
        [Test]
        public void Test7VParallelepiped()
        {
            double a = 4.3;
            double b = 2.4;
            double c = -2.9;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c));
        }
        [Test]
        public void TestAll()
        {
            double r = 2;
            double h = 7;
            double r1 = 2.3;
            double r2 = 4;
            double s = 9;
            double s1 = 4;
            double s2 = 8;
            double a = 8;
            double b = 1;
            double c = 10;

            double expectedSphere = (32 * Math.PI) / 3;
            double resSphere = new TRPO_Lab6.Lib.Lib().VSphere(r);
            Assert.AreEqual(expectedSphere, resSphere);

            double expectedCylinder = 28 * Math.PI;
            double resCylinder = new TRPO_Lab6.Lib.Lib().VCylinder(r, h);
            Assert.AreEqual(expectedCylinder, resCylinder);

            double expectedCone = (28 * Math.PI) / 3;
            double resCone = new TRPO_Lab6.Lib.Lib().VCone(r, h);
            Assert.AreEqual(expectedCone, resCone);

            double expectedTruncatedCone = (21343 * Math.PI) / 300;
            double resTruncatedCone = new TRPO_Lab6.Lib.Lib().VTruncatedCone(r1, r2, h);
            Assert.AreEqual(expectedTruncatedCone, resTruncatedCone);

            double expectedPyramid = 21;
            double resPyramid = new TRPO_Lab6.Lib.Lib().VPyramid(s, h);
            Assert.AreEqual(expectedPyramid, resPyramid);

            double expectedTruncatedPyramid = (28 + 28 * Math.Sqrt(2) / 3);
            double resTruncatedPyramid = new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(s1, s2, h);
            Assert.AreEqual(expectedTruncatedPyramid, resTruncatedPyramid);

            double expected�ube = 343;
            double res�ube = new TRPO_Lab6.Lib.Lib().V�ube(h);
            Assert.AreEqual(expected�ube, res�ube);

            double expectedParallelepiped = 80;
            double resParallelepiped = new TRPO_Lab6.Lib.Lib().VParallelepiped(a, b, c);
            Assert.AreEqual(expectedParallelepiped, resParallelepiped);
        }
    }
}