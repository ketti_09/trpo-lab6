﻿using System.ComponentModel;
using System.Windows;
using System;
using System.Windows.Input;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace TRPO_Lab6.WPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("1) Введите в текстовые поля соответствующие размеры параметров фигуры для вычисления объема. Десятичные значения необходимо вводить через точку.\n\n2)Затем приложение автоматически рассчитает объем выбранной фигуры. Для обновления значения объема необходимо нажать на соответствующее текстовое поле.\n\n3) Все результаты расчетов объемов фигур можно сохранить в виде изображения: введите путь в соответствующее текстовое поле и нажмите на иконку 'Загрузить' справа.", "Справочная информация по использованию приложения");
        }

        public class MainWindowViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            //Объем шара
            private double rSphere = 1;
            public double RSphere
            {
                get { return rSphere; }
                set
                {
                    rSphere = value;
                    if (rSphere > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VSphere)));
                    else
                        MessageBox.Show("Радиус шара должен быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VSphere
            {
                get { return new TRPO_Lab6.Lib.Lib().VSphere(RSphere); }
            }

            //Объем цилиндра
            private double rCylinder = 1;
            public double RCylinder
            {
                get { return rCylinder; }
                set
                {
                    rCylinder = value;
                    if (rCylinder > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VCylinder)));
                    else
                        MessageBox.Show("Радиус основания цилиндра должен быть больше нуля!", "Неверно введено значение");
                }
            }
            private double hCylinder = 1;
            public double HCylinder
            {
                get { return hCylinder; }
                set
                {
                    hCylinder = value;
                    if (hCylinder > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VCylinder)));
                    else
                        MessageBox.Show("Высота цилиндра должена быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VCylinder
            {
                get { return new TRPO_Lab6.Lib.Lib().VCylinder(RCylinder, HCylinder); }
            }

            //Объем конуса
            private double rCone = 1;
            public double RCone
            {
                get { return rCone; }
                set
                {
                    rCone = value;
                    if (rCone > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VCone)));
                    else
                        MessageBox.Show("Радиус основания конуса должен быть больше нуля!", "Неверно введено значение");
                }
            }
            private double hCone = 1;
            public double HCone
            {
                get { return hCone; }
                set
                {
                    hCone = value;
                    if (hCone > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VCone)));
                    else
                        MessageBox.Show("Высота конуса должна быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VCone
            {
                get { return new TRPO_Lab6.Lib.Lib().VCone(RCone, HCone); }
            }

            //объем усеченного конуса
            private double r1TruncatedCone = 1;
            public double R1TruncatedCone
            {
                get { return r1TruncatedCone; }
                set
                {
                    r1TruncatedCone = value;
                    if (r1TruncatedCone > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VTruncatedCone)));
                    else
                        MessageBox.Show("Радиус нижнего основания усеченного конуса должен быть больше нуля!", "Неверно введено значение");
                }
            }
            private double r2TruncatedCone = 1;
            public double R2TruncatedCone
            {
                get { return r2TruncatedCone; }
                set
                {
                    r2TruncatedCone = value;
                    if (r2TruncatedCone > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VTruncatedCone)));
                    else
                        MessageBox.Show("Радиус верхнего основания усеченного конуса должен быть больше нуля!", "Неверно введено значение");
                }
            }
            private double hTruncatedCone = 1;
            public double HTruncatedCone
            {
                get { return hTruncatedCone; }
                set
                {
                    hTruncatedCone = value;
                    if (hTruncatedCone > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VTruncatedCone)));
                    else
                        MessageBox.Show("Высота усеченного конуса должна быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VTruncatedCone
            {
                get { return new TRPO_Lab6.Lib.Lib().VTruncatedCone(R1TruncatedCone, R2TruncatedCone, HTruncatedCone); }
            }

            //Объем куба
            private double hСube = 1;
            public double HСube
            {
                get { return hСube; }
                set
                {
                    hСube = value;
                    if (hСube > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VСube)));
                    else
                        MessageBox.Show("Высота ребра куба должна быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VСube
            {
                get { return new TRPO_Lab6.Lib.Lib().VСube(HСube); }
            }

            //Объем пирамиды
            private double sPyramid = 1;
            public double SPyramid
            {
                get { return sPyramid; }
                set
                {
                    sPyramid = value;
                    if (sPyramid > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VPyramid)));
                    else
                        MessageBox.Show("Площадь основания пирамиды должна быть больше нуля!", "Неверно введено значение");
                }
            }
            private double hPyramid = 1;
            public double HPyramid
            {
                get { return hPyramid; }
                set
                {
                    hPyramid = value;
                    if (hPyramid > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VPyramid)));
                    else
                        MessageBox.Show("Высота пирамиды должна быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VPyramid
            {
                get { return new TRPO_Lab6.Lib.Lib().VPyramid(SPyramid, HPyramid); }
            }

            //Объем усеченной пирамиды
            private double s1TruncatedPyramid = 1;
            public double S1TruncatedPyramid
            {
                get { return s1TruncatedPyramid; }
                set
                {
                    s1TruncatedPyramid = value;
                    if (s1TruncatedPyramid > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VTruncatedPyramid)));
                    else
                        MessageBox.Show("Площадь верхнего основания усеченной пирамиды должна быть больше нуля!", "Неверно введено значение");
                }
            }
            private double s2TruncatedPyramid = 1;
            public double S2TruncatedPyramid
            {
                get { return s2TruncatedPyramid; }
                set
                {
                    s2TruncatedPyramid = value;
                    if (s2TruncatedPyramid > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VTruncatedPyramid)));
                    else
                        MessageBox.Show("Площадь нижнего основания усеченной пирамиды должна быть больше нуля!", "Неверно введено значение");
                }
            }
            private double hTruncatedPyramid = 1;
            public double HTruncatedPyramid
            {
                get { return hTruncatedPyramid; }
                set
                {
                    hTruncatedPyramid = value;
                    if (hTruncatedPyramid > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VTruncatedPyramid)));
                    else
                        MessageBox.Show("Высота усеченной пирамиды должна быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VTruncatedPyramid
            {
                get { return new TRPO_Lab6.Lib.Lib().VTruncatedPyramid(S1TruncatedPyramid, S2TruncatedPyramid, HTruncatedPyramid); }
            }

            //Объем параллелепипеда
            private double aParallelepiped = 1;
            public double AParallelepiped
            {
                get { return aParallelepiped; }
                set
                {
                    aParallelepiped = value;
                    if (aParallelepiped > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VParallelepiped)));
                    else
                        MessageBox.Show("Ребро 1 параллелепипеда должно быть больше нуля!", "Неверно введено значение");
                }
            }
            private double bParallelepiped = 1;
            public double BParallelepiped
            {
                get { return bParallelepiped; }
                set
                {
                    bParallelepiped = value;
                    if (bParallelepiped > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VParallelepiped)));
                    else
                        MessageBox.Show("Ребро 2 параллелепипеда должно быть больше нуля!", "Неверно введено значение");
                }
            }
            private double cParallelepiped = 1;
            public double CParallelepiped
            {
                get { return cParallelepiped; }
                set
                {
                    cParallelepiped = value;
                    if (cParallelepiped > 0)
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(VParallelepiped)));
                    else
                        MessageBox.Show("Ребро 3 параллелепипеда должно быть больше нуля!", "Неверно введено значение");
                }
            }
            public double VParallelepiped
            {
                get { return new TRPO_Lab6.Lib.Lib().VParallelepiped(AParallelepiped, BParallelepiped, CParallelepiped); }
            }
        }
        private void SaveImageVolume_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PathSaveImageVolume.Text[PathSaveImageVolume.Text.Length - 1] == Convert.ToChar("\\"))
                {
                    PathSaveImageVolume.Text = PathSaveImageVolume.Text.TrimEnd(Convert.ToChar("\\")) + "\\";
                }
                else if (PathSaveImageVolume.Text[PathSaveImageVolume.Text.Length - 1] != Convert.ToChar("\\"))
                {
                    PathSaveImageVolume.Text = PathSaveImageVolume.Text + "\\";
                }

                RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap(1280, 720, 0, 0, PixelFormats.Pbgra32);
                renderTargetBitmap.Render(Volumes);
                PngBitmapEncoder pngImage = new PngBitmapEncoder();
                pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                using (Stream fileStream = File.Create(PathSaveImageVolume.Text + "Объемы фигур.png"))
                {
                    pngImage.Save(fileStream);
                }
                MessageBox.Show("Результаты сохранёны: " + PathSaveImageVolume.Text + "Объемы фигур.png", "Изображение результатов успешно сохранено!");
            }
            catch
            {
                MessageBox.Show("Не удалось определить указанный путь!", "Сохранение изображения результатов не завершено!");
            }

        }

        private void PathSaveImageVolume_GotFocus(object sender, RoutedEventArgs e)
        {
            if (PathSaveImageVolume.Text == "Введите путь для сохранения изображения результатов")
                PathSaveImageVolume.Text = "";
        }

        private void PathSaveImageVolume_LostFocus(object sender, RoutedEventArgs e)
        {
            if (PathSaveImageVolume.Text == "")
                PathSaveImageVolume.Text = "Введите путь для сохранения изображения результатов";
        }
    }
}
