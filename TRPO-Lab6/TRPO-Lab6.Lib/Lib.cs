﻿using System;

namespace TRPO_Lab6.Lib
{
    public class Lib
    {
        public double VSphere(double RSphere)
        {
            if (RSphere > 0)
                return ((4 * Math.PI * Math.Pow(RSphere, 3)) / 3);
            else
            {
                if (RSphere == 0)
                    throw new ArgumentException("Значение r равно 0!");
                else
                    throw new ArgumentException("Значение r меньше 0!");
            }
        }
        public double VCylinder(double RCylinder, double HCylinder)
        {
            if (RCylinder > 0 && HCylinder > 0)
                return (Math.PI * Math.Pow(RCylinder, 2) * HCylinder);
            else
            {
                if (RCylinder == 0 || HCylinder == 0)
                    throw new ArgumentException("Значение r или h равно 0!");
                else
                    throw new ArgumentException("Значение r или h меньше 0!");
            }
        }
        public double VCone(double RCone, double HCone)
        {
            if (RCone > 0 && HCone > 0)
                return ((Math.PI * Math.Pow(RCone, 2) * HCone) / 3);
            else
            {
                if (RCone == 0 || HCone == 0)
                    throw new ArgumentException("Значение r или h равно 0!");
                else
                    throw new ArgumentException("Значение r или h меньше 0!");
            }
        }
        public double VTruncatedCone(double R1TruncatedCone, double R2TruncatedCone, double HTruncatedCone)
        {
            if (R1TruncatedCone > 0 && R2TruncatedCone > 0 && HTruncatedCone > 0)
                return ((Math.PI * HTruncatedCone * (Math.Pow(R1TruncatedCone, 2) + R1TruncatedCone * R2TruncatedCone + Math.Pow(R2TruncatedCone, 2))) / 3);
            else
            {
                if (R1TruncatedCone == 0 || R2TruncatedCone == 0 || HTruncatedCone == 0)
                    throw new ArgumentException("Значение r1 или r2 или h равно 0!");
                else
                    throw new ArgumentException("Значение r1 или r2 или h меньше 0!");
            }
        }
        public double VPyramid(double SPyramid, double HPyramid)
        {
            if (SPyramid > 0 && HPyramid > 0)
                return ((SPyramid * HPyramid) / 3);
            else
            {
                if (SPyramid == 0 || HPyramid == 0)
                    throw new ArgumentException("Значение s или h равно 0!");
                else
                    throw new ArgumentException("Значение s или h меньше 0!");
            }
        }
        public double VTruncatedPyramid(double S1TruncatedPyramid, double S2TruncatedPyramid, double HTruncatedPyramid)
        {
            if (S1TruncatedPyramid > 0 && S2TruncatedPyramid > 0 && HTruncatedPyramid > 0)
                return ((HTruncatedPyramid * (S1TruncatedPyramid + Math.Sqrt(S1TruncatedPyramid * S2TruncatedPyramid) + S2TruncatedPyramid)) / 3);
            else
            {
                if (S1TruncatedPyramid == 0 || S2TruncatedPyramid == 0 || HTruncatedPyramid == 0)
                    throw new ArgumentException("Значение s1 или s2 или h равно 0!");
                else
                    throw new ArgumentException("Значение s1 или s2 или h меньше 0!");
            }
        }
        public double VСube(double HСube)
        {
            if (HСube > 0)
                return (Math.Pow(HСube, 3));
            else
            {
                if (HСube == 0)
                    throw new ArgumentException("Значение h равно 0!");
                else
                    throw new ArgumentException("Значение h меньше 0!");
            }
        }
        public double VParallelepiped(double a, double b, double c)
        {
            if (a > 0 && b > 0 && c > 0)
                return (a * b * c);
            else
            {
                if (a == 0 || b == 0 || c == 0)
                    throw new ArgumentException("Значение a или b или c равно 0!");
                else
                    throw new ArgumentException("Значение a или b или c меньше 0!");
            }
        }
    }
}
